#!/usr/bin/env python

import sys, string, sqlite3, datetime, json
import BaseHTTPServer
from SimpleHTTPServer import SimpleHTTPRequestHandler


sql_conn = sqlite3.connect('server.db')

def generate_ticket(type, vars):
    
    with open('templates/' + type + '.txt', 'r') as content_file:
        content = string.Template(content_file.read())
    ticket = content.substitute(vars)
    return ticket

def count_ticket():
    
    return 0

def get_count():

    return 0
    
def adjust_counts():
    
    return 0
    
def setup_films():
    
    return 0

def get_films(date):
    
    return 0
    
def handle_request(headers):
    print headers
    
    if headers['config'] == '1':
        if headers['type'] == 'listings':
            # Requesting a list of films, that means a client is starting up or refreshing
            code = 200 # OK
            return_type = "listing"
            # At this point I should be doing an sqlite lookup
            try:
                lookup_date = headers['date']
            except KeyError:
                lookup_date = str(datetime.date.today())
                
            c = sql_conn.cursor()
            print lookup_date
            results = c.execute('select * from films where date=\'' + lookup_date + '\'')
            #json.dumps(results)
            
            films = dict()
            
            try:
                for film in results.fetchall():
                    films[film[0]] = (dict([('date', film[1]), ('time', film[2]), ('title', film[3])]))
            except IndexError:
                print "SQL ERROR or empty database"
            c.close()
            
            response = json.dumps(films)
            
            #response = '{"1":{"title":"Cloud Atlas","time":"18:30"},"2":{"title":"Wreck-it Ralph","time":"21:00"}}'
            
        elif headers['type'] == 'counts':
            # Requesting re-assigning ticket counts
            print 'changing counts'
        elif headers['type'] == 'films':
            # Add/modify film titles
            print 'changing films'
            c = sql_conn.cursor()
            c.execute("insert into films(date, time, title) values(%(date), %(time), %(title))" % (headers['film_date'], headers['film_time'], headers['film_title']))
            sql_conn.commit()
        else:
            print "Unknown type", headers['type']
        
    
    elif headers['type'] == 'n': # Non-member ticket
        c = sql_conn.cursor()
        results = c.execute('select * from films where id=\'' + headers['film'] + '\'')
        this_film = results.fetchone()
        code = 200 # OK
        if this_film[4] == None:
	    line2 = ''
	else:
	    line2 = this_film[4]
	if this_film[5] == None:
	    line3 = ''
	else:
	    line3 = this_film[5]
        response = generate_ticket('ibm', dict(header=this_film[0],
                                    ticket_head='Imperial Cinema Presents',
                                    film_title=this_film[3],
                                    film_line2=line2,
                                    film_line3=line3,
                                    date=this_film[1],
                                    time=this_film[2],
                                    type='Non-Member',
                                    price='#4.00',
                                    id='1212 1212',
                                    number='001',
                                    website='www.imperialcinema.co.uk',
                                    tagline='Sponsored by KPMG'))
        return_type = 'ticket'
    elif headers['type'] == 'm': # Member ticket
        if headers['use-free'] == 1:
            # Look up and check if this is ok, if so, update the database and return a ticket
            # If not, return an error
            print "not implemented yet"
        elif headers['use-free'] == 0:
            # Free ticket not used (will only happen if specified not to use it)
            c = sql_conn.cursor()
            results = c.execute('select * from films where id=\'' + headers['film'] + '\'')
            this_film = results.fetchone()
            code = 200 # OK
            response = generate_ticket('basic', dict(header='A',
                                        ticket_head='Imperial Cinema Presents',
                                        film_title=this_film[3],
                                        film_line2='',
                                        film_line3='',
                                        date=this_film[1],
                                        time=this_film[2],
                                        type='Member',
                                        price='#3.00',
                                        id='1212 1212',
                                        number='001',
                                        website='www.imperialcinema.co.uk',
				      tagline='Sponsored by KPMG'))
        else:
            # Check if free ticket already used. If so, just issue a paid one.
            
            # If not used, free ticket use must be specified.
            response = ''
            code = 300
            return_type = "error-usefree" # Must specify if you want to use a free ticket
        
    else:
        response = "Dunno"
        code = 200
        return_type = "error"
    return response, code, return_type
    
    
def main():

    
    class HandlerClass(SimpleHTTPRequestHandler):
        def do_POST(self):
            # Work out what we're sending back
            response, code, return_type = handle_request(self.headers)
            # Now send it!
            self.send_response(code)
            self.send_header("Content-length", len(response))
            self.send_header("type", return_type)
            self.end_headers()
            self.wfile.write(response)
        
        def do_GET(self):
            response = "Do a POST instead"
            self.send_response(200)
            self.send_header("Content-length", len(response))
            self.send_header("Response-type","")
            self.end_headers()
            self.wfile.write(response)

    ServerClass  = BaseHTTPServer.HTTPServer
    Protocol = "HTTP/1.0"
    
    if sys.argv[1:]:
        port = int(sys.argv[1])
    else:
        port = 8000
    server_address = ('0.0.0.0', port)
    
    HandlerClass.protocol_version = Protocol
    httpd = ServerClass(server_address, HandlerClass)
    
    sa = httpd.socket.getsockname()
    httpd.serve_forever()
    
    return
	
if __name__ == "__main__":
    main()

