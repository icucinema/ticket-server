#!/usr/bin/env python
#
# ticket-client
#
# A simply http client to request and print tickets
#
# depends on httplib2 (sudo apt-get install python-httplib2)

import os, sys, urllib, httplib2, json, datetime, serial
from StringIO import StringIO

def request_member(free):
    if free == '1':
        # Use free ticket if we can
        print 'b'
    elif free == '0':
        # Specified not using free ticket
        print 'a'
    else:
        # Check if free ticket applicable
        print 'c'
    return
    
def request_nonmember():
    
    return
    



def main():
    
    serial_port_device = "/dev/ttyUSB0"
    serial_port = serial.Serial(port=serial_port_device, baudrate=19200)
    
    http = httplib2.Http()
    
    if sys.argv[1:]:
        url = sys.argv[1]
    else:
        url = 'http://eastcote:8000'
    
    setup_headers = {'Content-type': 'application/x-www-form-urlencoded',
                   'config' : '1',
                   'type' : 'listings'}
    response, content = http.request(url, "POST", headers=setup_headers)
    #print content
    film_list = json.load(StringIO(content))
    #print film_list
    
    print "\nTonight's Films\n---------------"
    
    
    
    while 1:
        os.system('clear')
        for film in film_list:
            print film, ':', film_list[film]['time'], film_list[film]['title']
        
        chosen_film = raw_input("Choose film #:")
        
        current_type = raw_input("Ticket type.\nPress m for member, n for non-member:")
        
        if current_type != 'n':
            cid = raw_input("Enter login, CID or swipe card")
            
        else:
            cid = "99999999"
        headers = {'Content-type': 'application/x-www-form-urlencoded',
                   'CID' : str(cid),
                   'type' : current_type,
                   'film' : chosen_film,
                   'config' : '0',
                   'use-free' : ''}
        response, content = http.request(url, "POST", headers=headers)
        
        #print response
        print content
        serial_port.write(chr(0x1B)+chr(0x70)+chr(0x02)+chr(0x1F)+chr(0x0F))
        serial_port.write(content)
    return 0

if __name__ == "__main__":
    main()

